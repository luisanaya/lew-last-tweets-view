jQuery(document).ready(function(){
	jQuery('.show-tweets').fadeIn(700);
	var show_tweets = jQuery('.show-tweets').newsTicker({
		row_height: 60,
		max_rows: 1,
		speed: 300,
		duration: 6000,
		prevButton: '',
		nextButton: '',
		hasMoved: function() {},
		pause: function() {jQuery('.show-tweets li i').removeClass('fa-play').addClass('fa-pause');},
		unpause: function() {jQuery('.show-tweets li i').removeClass('fa-pause').addClass('fa-play');}
	});
});