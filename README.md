# README #

Muestra tus últimos Tweets en una barra horizontal y configúralo de la manera más conveniente.

### What is this repository for? ###

* Last tweets viewer
* Version: 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Utiliza las configuraciones del plugin y muestra los ultimos tweets, debes crear una aplicacion de twitter previamente para obtener los datos de autenticación.

### Acerca del desarrollador ###

* Autor: Luis Anaya | La Ã‰lite Web
* Contacto: luisanaya@laeliteweb.com
* www.laeliteweb.com