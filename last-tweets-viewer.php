<?php
/*
Plugin Name: Last Tweeter Viewer
Description: Permite ver una lista de Tweets en una barra estilo noticias, es un slide con movimiento vertical
Version: 0.1
Author: Luis Anaya
Author URI: http://luisanaya.com
License: Creative Commons
*/

require_once 'TweetPHP.php';

add_action('wp_enqueue_scripts', 'tweets_register_scripts');
add_action('wp_enqueue_scripts', 'tweets_register_styles');
		
function tweets_register_styles() {
	wp_enqueue_style('tweets_styles', plugin_dir_url( __FILE__ ).'css/style.css', '', '0.1', 'all');
}

function tweets_register_scripts() {
	wp_enqueue_script('newsTicker', plugin_dir_url( __FILE__ ).'js/jquery.newsTicker.min.js', array('jquery'), '0.1', false);
	wp_enqueue_script('tweets_init', plugin_dir_url( __FILE__ ).'js/init.js', array('jquery','newsTicker'), '0.1', false);
}

add_action( 'widgets_init', 'last_tweets_viewer_register_widgets' );
function last_tweets_viewer_register_widgets() {
	register_widget('last_tweets_viewer');
}

if (!class_exists('last_tweets_viewer')) {
	class last_tweets_viewer extends WP_Widget {
		function last_tweets_viewer() {
			$widget_ops = array('classname' => 'last_tweets_viewer', 'description' => 'Tweets Flash bar');
			$control_ops = array('width' => 300, 'height' => 350);
			$this->WP_Widget('last_tweets_viewer', 'Last Tweets Viewer', $widget_ops, $control_ops);
		}

		function widget($args, $instance) {
			$args = array(
				'consumer_key' => $instance['consumer_key'],
				'consumer_secret' => $instance['consumer_secret'],
				'access_token' => $instance['access_token'],
				'access_token_secret' => $instance['access_token_secret'],
				'usuario' => $instance['usuario'],
				'time_cache' => (int)$instance['time_cache'],
			);
			
			$TweetPHP = new TweetPHP(array(
			  'consumer_key'        => $args['consumer_key'],
			  'consumer_secret'     => $args['consumer_secret'],
			  'access_token'        => $args['access_token'],
			  'access_token_secret' => $args['access_token_secret'],
			  'twitter_screen_name' => $args['usuario'],
			  'cachetime'			=> $args['time_cache'],
			  'date_lang'			=> 'es',
			));
			
			$tweets = array();
			$tweet_array = $TweetPHP->get_tweet_array();
			
			echo '<div class="tweets-container">
				<ul class="show-tweets">';
				foreach($tweet_array as $tweet){
					$fecha = strtotime($tweet['created_at']);
					echo '<li><i class="fa fa-fw fa-play state"></i><span class="hour hidden-xs">'.date('d/m/Y h:i A',$fecha).'</span> | <a href="https://twitter.com/brusMX/status/'.$tweet['id_str'].'" target="_blank">'.$tweet['text'].'</a></li>';
				}
			echo '</ul>
			</div>';	
		}
		
		function form($instance) {
			$instance = wp_parse_args((array) $instance, array(
				'consumer_key' => '',
				'consumer_secret' => '',
				'access_token' => '',
				'access_token_secret' => '',
				'usuario' => '',
				'time_cache' => '900',
			));
			$type['consumer_key'] = strip_tags($instance['consumer_key']);
			$type['consumer_secret'] = strip_tags($instance['consumer_secret']);
			$type['access_token'] = strip_tags($instance['access_token']);
			$type['access_token_secret'] = strip_tags($instance['access_token_secret']);
			$type['usuario'] = strip_tags($instance['usuario']);
			$type['time_cache'] = strip_tags($instance['time_cache']);
			?>
			<p>
				<label for="<?php echo $this->get_field_id('consumer_key'); ?>"><?php echo __('Consumer Key', 'tweet_widget'); ?>:</label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id('consumer_key'); ?>" name="<?php echo $this->get_field_name('consumer_key'); ?>" value="<?php echo $type['consumer_key']; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('consumer_secret'); ?>"><?php echo __('Consumer Secret', 'tweet_widget'); ?>:</label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id('consumer_secret'); ?>" name="<?php echo $this->get_field_name('consumer_secret'); ?>" value="<?php echo $type['consumer_secret']; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('access_token'); ?>"><?php echo __('Access Token', 'tweet_widget'); ?>:</label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id('access_token'); ?>" name="<?php echo $this->get_field_name('access_token'); ?>" value="<?php echo $type['access_token']; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('access_token_secret'); ?>"><?php echo __('Access Token Secret', 'tweet_widget'); ?>:</label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id('access_token_secret'); ?>" name="<?php echo $this->get_field_name('access_token_secret'); ?>" value="<?php echo $type['access_token_secret']; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('usuario'); ?>"><?php echo __('Usuario de Tweeter', 'tweet_widget'); ?>:</label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id('usuario'); ?>" name="<?php echo $this->get_field_name('usuario'); ?>" value="<?php echo $type['usuario']; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('time_cache'); ?>"><?php echo __('Tiempo de caché', 'tweet_widget'); ?>:</label>
				<select class="widefat" id="<?php echo $this->get_field_id('time_cache'); ?>" name="<?php echo $this->get_field_name('time_cache'); ?>">
					<option value="900" <?php selected($type['time_cache'],'900'); ?>>15 minutos</option>
					<option value="1800" <?php selected($type['time_cache'],'1800'); ?>>30 minutos</option>
					<option value="2700" <?php selected($type['time_cache'],'2700'); ?>>45 minutos</option>
					<option value="3600" <?php selected($type['time_cache'],'3600'); ?>>1 hora</option>
					<option value="5400" <?php selected($type['time_cache'],'5400'); ?>>1:30 horas</option>
				</select>
			</p>
			<?php
		}
		
		function update($new_instance, $old_instance) {
			$instance = $old_instance;
			$instance['consumer_key'] = strip_tags($new_instance['consumer_key']);
			$instance['consumer_secret'] = strip_tags($new_instance['consumer_secret']);
			$instance['access_token'] = strip_tags($new_instance['access_token']);
			$instance['access_token_secret'] = strip_tags($new_instance['access_token_secret']);
			$instance['usuario'] = strip_tags($new_instance['usuario']);
			$instance['time_cache'] = strip_tags($new_instance['time_cache']);
			return $instance;
		}
	}
}
//Developed By Luis Anaya